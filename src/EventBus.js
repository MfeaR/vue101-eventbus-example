import Vue from 'vue';
import { ToastPlugin } from 'bootstrap-vue';

Vue.use(ToastPlugin);
const EventBus = new Vue();
export default EventBus;
