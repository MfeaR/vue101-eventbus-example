function createDB() {
  const tasks = [{ name: 'Написать проект' }];
  const orders = [
    { name: 'Ботинки', cost: 21 },
    { name: 'Туфли', cost: 15 },
    { name: 'Красовки', cost: 44 },
  ];
  localStorage.setItem('DB', JSON.stringify({ tasks, orders }));
}

export function testDB() {
  try {
    const { tasks, orders } = JSON.parse(localStorage.getItem('DB'));
    tasks.find(({ name }) => name);
    orders.find(({ name, cost }) => name && cost);
  } catch (e) {
    createDB();
  }
}
