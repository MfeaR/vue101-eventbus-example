import Request from '@/api/request';

const ApiTasks = new Request('tasks');
const ApiOrders = new Request('orders');

export { ApiTasks, ApiOrders };
