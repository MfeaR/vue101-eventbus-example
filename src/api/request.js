import EventBus from '@/EventBus';

export default class Request {
  constructor(model) {
    this.model = model;
  }
  get() {
    return new Promise((resolve, reject) => {
      try {
        const DB = JSON.parse(localStorage.getItem('DB'));
        resolve(DB[this.model]);
      } catch (e) {
        EventBus.$bvToast.toast(e, { title: 'Ошибка', variant: 'danger' });
        reject(e);
      }
    });
  }
  create(data) {
    return new Promise((resolve, reject) => {
      try {
        const DB = JSON.parse(localStorage.getItem('DB'));
        DB[this.model].push(data);
        localStorage.setItem('DB', JSON.stringify(DB));
        resolve('success');
      } catch (e) {
        EventBus.$bvToast.toast(e, { title: 'Ошибка', variant: 'danger' });
        reject(e);
      }
    });
  }
}
