import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import App from './App.vue';
import router from './router';

import '@/assets/styles.scss';
import { testDB } from '@/api/db';

Vue.config.productionTip = false;

testDB();

Vue.use(BootstrapVue);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
