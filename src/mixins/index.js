import EventBus from '@/EventBus';

export const RegEventBusMixin = {
  name: 'RegEventBusMixin',
  data() {
    return {
      eventMixinsArray: [],
    };
  },
  methods: {
    registerInEventBus(name, callback) {
      if (this.eventMixinsArray.includes(name)) return;
      this.eventMixinsArray.includes(name);
      EventBus.$on(name, callback);
      this.eventMixinsArray.push(name);
    },
  },
  beforeDestroy(to, from, next) {
    EventBus.$off(this.eventMixinsArray);
    return next();
  },
};
