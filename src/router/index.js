import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '@/layouts/Home.vue';
import Tasks from '@/views/Tasks.vue';
import Orders from '@/views/Orders.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    redirect: { name: 'tasks' },
    component: Home,
    children: [
      {
        path: 'tasks',
        name: 'tasks',
        component: Tasks,
      },
      {
        path: 'orders',
        name: 'orders',
        component: Orders,
      },
    ],
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
